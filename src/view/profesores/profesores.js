new Vue({
    el: '#app-profesores',
    data:{
        titulo: 'Gestión de Profesores',
        profesores:[],
        color: true,
        editarProfesor:{
            "nombres": null,
            "email": null,
            "newEmail": null,
            "idusuario": null,
            "carga": null,
            "acceso": null,
            "notaCarga":null
        },
        resetPasswordProf:null,
        cargaTrimestre:0
    },
    methods: {
        getProfesores(){
            axios.get('backend/getProfesores.php')
            .then(resp=>{
                this.profesores=resp.data
                this.cargaTrimestre=this.profesores[0].notaCarga
                console.log(this.profesores)
            })
            .catch(e=>{
                console.log(e)
            })
        },
        enviarMail(mail){
            console.log(mail)
            if(mail===undefined){
               mail=null 
            }
            let data={
                "mail": mail
            }
            axios.post('backend/enviarMail.php', data)
            .then(resp=>{
                console.log(resp.data)
                let alerta = document.getElementById('alerta')
                if(resp.data.error){
                    alerta.innerHTML=`
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            ${resp.data.message}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>`
                }else{
                    alerta.innerHTML=`
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        ${resp.data.message}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>`
                }
                
            })
            .catch(e=>{
                console.log(e)
            })
        },
        cargaAll(val){
            console.log(val)
            let data = {"carga":val}
            axios.post('backend/setCargaAll.php', data)
            .then(resp=>{
                console.log(resp.data)
                let alerta = document.getElementById('alerta')
                if(resp.data.error){
                    alerta.innerHTML=`
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            ${resp.data.message}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>`
                }else{
                    alerta.innerHTML=`
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        ${resp.data.message}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>`
                }
                
            })
            .catch(e=>{
                console.log(e)
            })
            this.profesores=[]
            this.getProfesores()
        },
        carga(val,usuario){
            console.log(val,usuario)
            let mail=usuario.email
            console.log(mail)
            let data = {"carga":val,"mail":mail}
            axios.post('backend/setCarga.php', data)
            .then(resp=>{
                console.log(resp.data)
                let alerta = document.getElementById('alerta')
                if(resp.data.error){
                    alerta.innerHTML=`
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            ${resp.data.message}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>`
                }else{
                    alerta.innerHTML=`
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        ${resp.data.message}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>`
                }
                
            })
            .catch(e=>{
                console.log(e)
            })
            this.profesores=[]
            this.getProfesores()
        },
        editarMail(prof){
            this.editarProfesor=prof
            console.log(this.editarProfesor)
        },
        confCambioMail(){
            console.log('Cambiar email:', this.editarProfesor)
            let data = this.editarProfesor
            axios.post('backend/editarMail.php',data)
            .then(resp=>{
                console.log(resp.data)
                let alerta = document.getElementById('alerta')
                if(resp.data.error){
                    alerta.innerHTML=`
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            ${resp.data.message}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>`
                }else{
                    alerta.innerHTML=`
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        ${resp.data.message}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>`
                }
            })
            .catch(e=>{
                console.log(e)
            })
            this.profesores=[]
            this.getProfesores()
        },
        resetPassword(prof){
            this.resetPasswordProf=prof
            console.log(this.resetPasswordProf)
        },
        confirmReset(){
            data={
                "idusuarios":this.resetPasswordProf.idusuarios
            }
            axios.post('backend/resetPassword.php', data)
            .then(resp=>{
                console.log(resp.data)
                let alerta = document.getElementById('alerta')
                if(resp.data.error){
                    alerta.innerHTML=`
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            ${resp.data.message}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>`
                }else{
                    alerta.innerHTML=`
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        ${resp.data.message}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>`
                }
                this.resetPasswordProf=null
            })
            .catch(e=>{
                console.log(e)
            })
        },
        setInstaciaCarga(){
            console.log('guardar instancia:', this.cargaTrimestre)
            let data={
                "notaCarga":this.cargaTrimestre
            }
            axios.post('backend/setInstanciaCarga.php', data)
            .then(resp=>{
                console.log(resp.data)
                let alerta = document.getElementById('alerta')
                if(resp.data.error){
                    alerta.innerHTML=`
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            ${resp.data.message}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>`
                }else{
                    alerta.innerHTML=`
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        ${resp.data.message}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>`
                }
            })
            .catch(e=>{
                console.log(e)
            })
        }
    },
    created() {
        this.profesores=[]
        this.getProfesores()
    },
})