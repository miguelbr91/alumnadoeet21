$(document).ready(function(){
    fetch('backend/session.php',{
       method: 'get',
    })
    .then(res=>res.json())
    .then(data=>{
        let ususario=data
        let formularios=document.getElementById('nav-formularios')
        let cursos=document.getElementById('nav-cursos')
        let profesores=document.getElementById('nav-profesores')
        if(ususario.nivel==='0'){
            formularios.classList.remove('d-none')
            cursos.classList.add('d-none')
            profesores.classList.add('d-none')
        }
        if(ususario.nivel==='2'){
            formularios.classList.add('d-none')
            cursos.classList.remove('d-none')
            profesores.classList.remove('d-none')
        }

    })
});

function navActive(item){
    switch(item){
        case 'home':$('#nav-cargaNotas').addClass('active');
                    $('#nav-formularios').removeClass('active');
                    $('#nav-cursos').removeClass('active');
                    $('#nav-profesores').removeClass('active');
                    break;
        case 'formularios': $('#nav-cargaNotas').removeClass('active');
                            $('#nav-formularios').addClass('active');
                            $('#nav-cursos').removeClass('active');
                            $('#nav-profesores').removeClass('active');break;
        case 'cursos':  $('#nav-cargaNotas').removeClass('active');
                        $('#nav-formularios').removeClass('active');
                        $('#nav-cursos').addClass('active');
                        $('#nav-profesores').removeClass('active');
                        break;
        case 'profesores':  $('#nav-cargaNotas').removeClass('active');
                            $('#nav-formularios').removeClass('active');
                            $('#nav-cursos').removeClass('active');
                            $('#nav-profesores').addClass('active');
                            break;
    }
}


$('#cargaNotas').click(function(){
    $('#container-data').load('view/home/home.html');
    navActive('home');
});
$('#formularios').click(function(){
    $('#container-data').load('view/formularios/formularios.html');
    navActive('formularios');
});
$('#cursos').click(function(){
    $('#container-data').load('view/cursos/cursos.html');
    navActive('cursos');
});
$('#profesores').click(function(){
    $('#container-data').load('view/profesores/profesores.html');
    navActive('profesores');
});