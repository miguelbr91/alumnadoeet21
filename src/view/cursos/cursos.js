new Vue({
    el: '#app-cursos',
    data:{
        titulo: 'Gestion de Notas por Cursos',
        cursos:{
            "esp":"",
            "cur":""
        },
        listaCursos:[]
    },
    methods: {
        getCursos(){
            let data=this.cursos
            console.log(data)
            axios.post('backend/getCursos.php', data)
            .then(resp=>{
                console.log(resp.data)
                this.listaCursos=resp.data
            })
            .catch(e=>{
                console.log(e)
            })
        }
    },
})