<?php
    include_once "conn.php";
    $esp=$_POST['esp'];
    $curso=$_POST['curso'];
    $div=$_POST['div'];
    $trim=$_POST['trimestre'];

    $sql="SELECT DISTINCT(AlumnoDNI),Apellidos,Nombres,CicloLectivo FROM NotasCurso WHERE CURSO=? AND Division=? AND ESPECIALIDAD=? ORDER BY Apellidos,Nombres";
    $alum_sql=$pdo->prepare($sql);
    $alum_sql->execute(array($curso,$div,$esp));
    $alum=$alum_sql->fetchAll();

    $sql="SELECT * FROM especialidad WHERE `CodEspecialidad`=?";
    $esp_sql=$pdo->prepare($sql);
    $esp_sql->execute(array($esp));
    $esp_reg=$esp_sql->fetchAll();

    $sql="SELECT DISTINCT(`Nc`.`CodMat`),`Nc`.`Mat_Nombre`,`Nc`.`Orden` FROM NotasCurso as Nc WHERE Nc.CURSO=? AND Nc.Division=? AND Nc.ESPECIALIDAD=? order by Nc.Orden";
    $mat_sql=$pdo->prepare($sql);
    $mat_sql->execute(array($curso,$div,$esp));
    $mat=$mat_sql->fetchAll();

    function AlumnoNombres($n){
        $array = explode(" ", $n);
        return $array[0];
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Estilos CSS -->
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/printLegal.css">
    <title>Imprimir Notas</title>
</head>
<body>
    <div id="app">
        <div class="container">
            <div class="row my-3">
                <form class="form mx-auto" action="" method="post" target="_blank">
                    <input type="hidden" name="data" :value="impresion">
                    <button type="button" class="btn btn-primary" id="btnPrint" @click="imprimir()"><i class="fas fa-print"></i> Imprimir</button>
                </form>
            </div>
        </div>
        <div class="container" id="forma">
            <!-- cabecera de notas -->
            <div class="row border p-1">
                <div class="col-12">
                    <h5 class="text-center text-uppercase">MINISTERIO DE EDUCACION, CULTURA, CIENCIA Y TECNOLOGIA</h5>
                    <h5 class="text-center text-uppercase">E.E.T. N° 21 - GENERAL MANUEL BELGRANO</h5>
                </div>
            </div>

            <!-- Encabezado Curso y Materia -->
            <div class="row mt-2">
                <div class="col-12">
                    <table class="table-print table-bordered">
                        <thead class="table-secondary">
                            <tr>
                                <th class="text-center">Especialidad</th>
                                <th class="text-center">Curso</th>
                                <th class="text-center">Div</th>
                                <th class="text-center">Año</th>
                                <th class="text-center">Notas</th>
                                <th class="text-center">Emision</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center"><?php echo $esp_reg[0]['Abreviatura'] ?></td>
                                <td class="text-center"><?php echo $curso ?></td>
                                <td class="text-center"><?php echo $div ?></td>
                                <td class="text-center"><?php echo $alum[0]['CicloLectivo'] ?></td>
                                <td class="text-center">
                                    <?php 
                                        switch ($trim) {
                                            case 1:
                                                echo "$trim T";
                                                break;
                                            case 2:
                                                # code...
                                                echo "$trim T";
                                                break;
                                            case 3:
                                                # code...
                                                echo "$trim T";
                                                break;
                                            case 4:
                                                # code...
                                                echo "C Final";
                                                break;
                                            case 5:
                                                # code...
                                                echo "Diciembre";
                                                break;
                                            case 6:
                                                # code...
                                                echo "Marzo";
                                                break;
                                            case 7:
                                                # code...
                                                echo "Nota Final";
                                                break;
                                        }
                                    ?>
                                </td>
                                <td class="text-center"><?php echo date("d/m/Y") ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- Lista de Alumnos y notas -->

            <div class="row mt-2">
                <div class="col-12">
                    <table class="table-print table-bordered">
                        <thead class="table-secondary table-materia">
                            <tr>
                                <th class="text-center font-th">#</th>
                                <th class="text-center font-th">Doc N°</th>
                                <th class="text-center font-th">Apellido y Nombre</th>
                                <?php
                                    for ($j=0; $j < sizeof($mat); $j++) { 
                                ?>
                                    <th class="text-vertical text-center font-th"><?php echo substr($mat[$j]['Mat_Nombre'],0,20).'.'; ?></th>
                                <?php        
                                    }
                                ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                for ($i=0; $i < sizeof($alum); $i++) { 
                            ?>
                                <tr>
                                    <td class="text-center font-td"><?php echo $i+1 ?></td>
                                    <td class="text-center font-td"><?php echo $alum[$i]['AlumnoDNI'] ?></td>
                                    <td class="text-left font-td">
                                        <?php 
                                            $firstName=AlumnoNombres($alum[$i]['Nombres']);
                                            $nombre=$alum[$i]['Apellidos'].', '.$firstName;
                                            if(strlen($nombre)>20){
                                                echo substr($nombre,0,20).'...';
                                            }else{
                                                echo $nombre;
                                            }
                                        ?>
                                    </td>
                                    <?php
                                        $dni=$alum[$i]['AlumnoDNI'];
                                        $sql="SELECT Nota1T,Nota2T,Nota3T,Cfin,Dic,Mar,NotaFinal,CodMat FROM NotasCurso WHERE AlumnoDNI=? ORDER BY Orden";
                                        $notas_sql=$pdo->prepare($sql);
                                        $notas_sql->execute(array($dni));
                                        $notas=$notas_sql->fetchAll();
                                        for ($k=0; $k < sizeof($mat); $k++) { 
                                            switch ($trim) {
                                                case 1:
                                                ?>
                                                    <td class="text-center font-td font-weight-bold <?php if($notas[$k]['Nota1T']<6){ echo "text-danger"; } ?>"><?php if($notas[$k]['Nota1T']>0){echo $notas[$k]['Nota1T'];} ?></td>                                                    
                                                <?php
                                                    # code...
                                                    break;
                                                case 2:
                                                ?>
                                                    <td class="text-center font-td font-weight-bold <?php if($notas[$k]['Nota2T']<6){ echo "text-danger"; } ?>"><?php if($notas[$k]['Nota2T']>0){echo $notas[$k]['Nota2T'];} ?></td>
                                                <?php
                                                    # code...
                                                    break;
                                                case 3:
                                                ?>
                                                    <td class="text-center font-td font-weight-bold <?php if($notas[$k]['Nota3T']<6){ echo "text-danger"; } ?>"><?php if($notas[$k]['Nota3T']>0){echo $notas[$k]['Nota3T'];} ?></td>
                                                <?php
                                                    # code...
                                                    break;
                                                case 4:
                                                ?>
                                                    <td class="text-center font-td font-weight-bold <?php if($notas[$k]['CFin']<6 || $notas[$k]['Nota3T']<6 || ( ($notas[$k]['CodMat']=='2') && ( $notas[$k]['Nota1T']<6 || $notas[$k]['Nota2T']<6 ) ) ){ echo "text-danger"; } ?>"><?php if($notas[$k]['CFin']>0){echo $notas[$k]['CFin'];} ?></td>
                                                <?php
                                                    # code...
                                                    break;
                                                case 5:
                                                ?>
                                                    <td class="text-center font-td font-weight-bold <?php if($notas[$k]['Dic']<6){ echo "text-danger"; } ?>"><?php if($notas[$k]['Dic']>0){echo $notas[$k]['Dic'];} ?></td>
                                                <?php
                                                    # code...
                                                    break;
                                                case 6:
                                                ?>
                                                    <td class="text-center font-td font-weight-bold <?php if($notas[$k]['Mar']<6){ echo "text-danger"; } ?>"><?php if($notas[$k]['Mar']>0){echo $notas[$k]['Mar'];} ?></td>
                                                <?php
                                                    # code...
                                                    break;
                                                case 7:
                                                ?>
                                                    <td class="text-center font-td font-weight-bold"><?php if($notas[$k]['NotaFinal']>0){echo $notas[$k]['NotaFinal'];} ?></td>
                                                <?php
                                                    # code...
                                                    break;
                                                
                                            }     
                                        }
                                    ?>
                                    
                                </tr>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- development version, includes helpful console warnings -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <!-- production version, optimized for size and speed
    <script src="https://cdn.jsdelivr.net/npm/vue"></script> -->
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script type="text/javascript">
        new Vue({
            el: '#app',
            data:{
                impresion: ''
            },
            methods: {
                imprimir(){
                    // let printContents = this.impresion
                    // w = window.open();
                    // w.document.write(printContents);
                    // w.document.close();
                    // w.focus();
                    // w.print();
                    // w.close();
                    // return true;
                    window.print()
                }
            },
        })
    </script>
</body>
</html>