<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    include_once "conn.php";
    $idusuarios=$data['idusuarios'];
    $newPass=$data['new'];

    $password=password_hash($newPass, PASSWORD_DEFAULT);
    $sql="UPDATE usuarios SET `password`=?,`acceso`=1 WHERE `idusuarios`=?";
    $update_sql=$pdo->prepare($sql);
    $update_sql->execute(array($password,$idusuarios));

    if($update_sql){
        session_start();
        $_SESSION['type']=1;
        $response=[
            "message"=>"<strong>Correcto!</strong> La contraseña se modificó correctamente.",
            "error"=>false
        ];
    }else{
        $response=[
            "message"=>"<strong>Error!</strong> Ocurrió un error en el proceso, vuelva a intentar.",
            "error"=>true
        ];
    }
    echo json_encode($response)
?>