<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    include_once "conn.php";
    $id=$data['idusuarios'];
    $nombres=$data['nombres'];
    $new=$data['newEmail'];
    $old=$data['email'];
    $notaCarga=$data['notaCarga'];
    $key='eet21';

    
    $sql="SELECT * FROM usuarios WHERE  mail=?";
    $select_sql=$pdo->prepare($sql);
    $select_sql->execute(array($new));
    $mail=$select_sql->fetchAll();
    if($mail[0]['mail']!=null){
        $update_sql=true;
    }else{
        // $sql="UPDATE usuarios SET `mail`=? WHERE `idusuarios`=?";
        $sql="INSERT INTO usuarios(`mail`,`key`,`notaCarga`) values (?,?,?)";
        $update_sql=$pdo->prepare($sql);
        $update_sql->execute(array($new,$key,$notaCarga));
    }

    if($update_sql){
        $sql2="UPDATE profxmat2019 SET `Prof_Email`=? WHERE `Prof_ApellYnom`=?";
        $update2_sql=$pdo->prepare($sql2);
        $update2_sql->execute(array($new,$nombres));
        if($update2_sql){
            $response=[
                "message"=>"<strong>Correcto!</strong> El e-mail se modificó correctamente.",
                "error"=>false
            ];
        }else{
            $response=[
                "message"=>"<strong>Error!</strong> Ocurrió un error en el proceso, vuelva a intentar.",
                "error"=>true
            ];
        }
    }else{
        $response=[
            "message"=>"<strong>Error!</strong> Ocurrió un error en el proceso, vuelva a intentar.",
            "error"=>true
        ];
    }
    echo json_encode($response)
?>