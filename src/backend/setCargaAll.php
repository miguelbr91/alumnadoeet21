<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    $carga=$data['carga'];
    include_once "conn.php";
    if($carga){
        $sql="UPDATE usuarios SET carga=1";
        $message = "Correcto! La carga de notas ha sido habilitada.";
    }else{
        $sql="UPDATE usuarios SET carga=0";
        $message = "Correcto! La carga de notas ha sido deshabilitada.";
    }
    $cargaAll_sql=$pdo->prepare($sql);
    $cargaAll_sql->execute();

    if($cargaAll_sql){
        $error=false;
    }else{
        $error=true;
        $message = "Error! La carga de notas no pudo ser gestionada.";
    }
    $response=[
        "message"=>$message,
        "error"=>$error
    ];
    echo json_encode($response);
?>