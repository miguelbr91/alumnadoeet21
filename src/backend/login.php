<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    $user_mail=$data['mail'];
    $user_pass=$data['password'];
    include_once "conn.php";
    $sql="SELECT * FROM usuarios WHERE `mail`=?";
    $sesion_sql=$pdo->prepare($sql);
    $sesion_sql->execute(array($user_mail));
    $sesion_user=$sesion_sql->fetch();
    if(!empty($sesion_user)){
        if($sesion_user['password']!=null){
            if(password_verify($user_pass,$sesion_user['password'])){
                $usuario=[
                    "idusuarios" => $sesion_user['idusuarios'],
                    "mail" => $sesion_user['mail'],
                    "type"=>1,
                    "nivel"=>$sesion_user['nivel'],
                    "carga"=>$sesion_user['carga'],
                    "notaCarga"=>$sesion_user['notaCarga']
                ];
                session_start();
                $_SESSION['idusuarios']=$usuario['idusuarios'];
                $_SESSION['mail']=$usuario['mail'];
                $_SESSION['type']=$usuario['type'];
                $_SESSION['nivel']=$usuario['nivel'];
                $_SESSION['carga']=$usuario['carga'];
                $_SESSION['notaCarga']=$usuario['notaCarga'];
                echo json_encode($usuario);
            }else{  
                echo json_encode(["message"=>"Contraseña incorrecta.", "error"=>true]);
            }
        }else{
            if($sesion_user['key']===$user_pass){
                $usuario=[
                    "idusuarios" => $sesion_user['idusuarios'],
                    "mail" => $sesion_user['mail'],
                    "type"=> 0,
                    "nivel"=>$sesion_user['nivel'],
                    "carga"=>$sesion_user['carga'],
                    "notaCarga"=>$sesion_user['notaCarga']
                ];
                session_start();
                $_SESSION['idusuarios']=$usuario['idusuarios'];
                $_SESSION['mail']=$usuario['mail'];
                $_SESSION['type']=$usuario['type'];
                $_SESSION['nivel']=$usuario['nivel'];
                $_SESSION['carga']=$usuario['carga'];
                $_SESSION['notaCarga']=$usuario['notaCarga'];
                echo json_encode($usuario);
            }else{
                echo json_encode(["message"=>"Contraseña incorrecta.", "error"=>true]);
            }
        }
    }else{
        echo json_encode(["message"=>"El usuario no existe.", "error"=>true]);
    }
?>