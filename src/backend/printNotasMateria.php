<?php
    include_once "conn.php";
    $esp=$_POST['esp'];
    $curso=$_POST['curso'];
    $div=$_POST['div'];
    $codmat=$_POST['codmat'];
    $nombreMat=$_POST['nombreMat'];

    $sql="SELECT * FROM NotasFinalAlumno WHERE `ESPECIALIDAD`=? AND `CURSO`=? AND `DIVISION` = ? AND `CodMat`=? order by `Apellidos`,`Nombres`";
    $alum_sql=$pdo->prepare($sql);
    $alum_sql->execute(array($esp,$curso,$div,$codmat));
    $alum=$alum_sql->fetchAll();

    $max=sizeof($alum);

    $sql="SELECT * FROM especialidad WHERE `CodEspecialidad`=?";
    $esp_sql=$pdo->prepare($sql);
    $esp_sql->execute(array($esp));
    $esp_reg=$esp_sql->fetchAll();

    
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Estilos CSS -->
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/printA4.css">
</head>
<body>
    <div id="app">
        <div class="container">
            <div class="row my-3">
                <form class="form mx-auto" action="" method="post" target="_blank">
                    <input type="hidden" name="data" :value="impresion">
                    <button type="button" class="btn btn-primary" id="btnPrint" @click="imprimir()"><i class="fas fa-print"></i> Imprimir</button>
                </form>
            </div>
        </div>
        <div class="container" id="forma">
            <!-- cabecera de notas -->
            <div class="row border p-1">
                <div class="col-12">
                    <h5 class="text-center text-uppercase font-16">MINISTERIO DE EDUCACION, CULTURA, CIENCIA Y TECNOLOGIA</h5>
                    <h5 class="text-center text-uppercase font-16">E.E.T. N° 21 - GENERAL MANUEL BELGRANO</h5>
                </div>
            </div>

            <!-- Encabezado Curso y Materia -->
            <div class="row mt-2">
                <div class="col-12">
                    <table class="table-print table-bordered">
                        <thead class="table-dark text-white">
                            <tr>
                                <th class="text-center font-14">Especialidad</th>
                                <th class="text-center font-14">Curso</th>
                                <th class="text-center font-14">Div</th>
                                <th class="text-center font-14">Año</th>
                                <th class="text-center font-14">Asignatura</th>
                                <th class="text-center font-14">Emision</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center font-12"><?php echo $esp_reg[0]['Abreviatura'] ?></td>
                                <td class="text-center font-12"><?php echo $curso ?></td>
                                <td class="text-center font-12"><?php echo $div ?></td>
                                <td class="text-center font-12"><?php echo $alum[0]['CicloLectivo'] ?></td>
                                <td class="text-center font-12"><?php echo $codmat.'  -  '.$nombreMat ?></td>
                                <td class="text-center font-12"><?php echo date("d/m/Y") ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- Lista de Alumnos y notas -->

            <div class="row mt-2">
                <div class="col-12">
                    <table class="table-print table-bordered">
                        <thead class="table-secondary">
                            <tr>
                                <th class="text-center">Doc N°</th>
                                <th class="text-center">Apellido y Nombre</th>
                                <th class="text-center">Condicion</th>
                                <th class="text-center">1 T</th>
                                <th class="text-center">2 T</th>
                                <th class="text-center">3 T</th>
                                <th class="text-center">C Fin</th>
                                <th class="text-center">Dic</th>
                                <th class="text-center">Mar</th>
                                <th class="text-center">C Def</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                for ($i=0; $i < $max; $i++) { 
                            ?>
                                <tr>
                                    <td class="text-center"><?php echo $alum[$i]['AlumnoDNI'] ?></td>
                                    <td class="text-left"><?php echo $alum[$i]['Apellidos'].', '.$alum[$i]['Nombres'] ?></td>
                                    <td class="text-center"><?php echo $alum[$i]['RegNombre'] ?></td>
                                    <td class="text-center font-weight-bold <?php if($alum[$i]['Nota1T']<6){ echo "text-danger"; } ?>"><?php if($alum[$i]['Nota1T']>0){echo $alum[$i]['Nota1T'];} ?></td>
                                    <td class="text-center font-weight-bold <?php if($alum[$i]['Nota2T']<6){ echo "text-danger"; } ?>"><?php if($alum[$i]['Nota2T']>0){echo $alum[$i]['Nota2T'];} ?></td>
                                    <td class="text-center font-weight-bold <?php if($alum[$i]['Nota3T']<6){ echo "text-danger"; } ?>"><?php if($alum[$i]['Nota3T']>0){echo $alum[$i]['Nota3T'];} ?></td>
                                    <td class="text-center font-weight-bold <?php  if(($alum[$i]['Nota3T']<6)||($alum[$i]['CFin']<6)){ echo "text-danger"; } ?>"><?php if($alum[$i]['CFin']>0){echo $alum[$i]['CFin'];} //$alum[$i]['CFin'] ?></td>
                                    <td class="text-center font-weight-bold <?php if(($alum[$i]['Dic']<6)){ echo "text-danger"; } ?>"><?php if($alum[$i]['Dic']>0){echo $alum[$i]['Dic'];} ?></td>
                                    <td class="text-center font-weight-bold"><?php echo ''//$alum[$i]['Mar'] ?></td>
                                    <td class="text-center font-weight-bold"><?php if($alum[$i]['NotaFinal']>0){echo $alum[$i]['NotaFinal'];}//$alum[$i]['NotaFinal'] ?></td>
                                </tr>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- development version, includes helpful console warnings -->
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <!-- production version, optimized for size and speed
    <script src="https://cdn.jsdelivr.net/npm/vue"></script> -->
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script type="text/javascript">
        new Vue({
            el: '#app',
            data:{
                impresion: ''
            },
            methods: {
                imprimir(){
                    // let printContents = this.impresion
                    // w = window.open();
                    // w.document.write(printContents);
                    // w.document.close();
                    // w.focus();
                    // w.print();
                    // w.close();
                    // return true;
                    window.print()
                }
            },
            created() {
                let forma=document.getElementById('forma')
                this.impresion=`<!DOCTYPE html>
                    <html lang="es">
                    <head>
                        <meta charset="UTF-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <meta http-equiv="X-UA-Compatible" content="ie=edge">
                        <!-- font awesome -->
                        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
                        <!-- bootstrap CSS -->
                        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
                        <!-- Estilos CSS -->
                        <link rel="stylesheet" href="../css/style.css">
                    </head>
                    <body>
                        ${forma.outerHTML}
                    </body>
                    </html>
                `
                console.log(this.impresion)
            },
        })
    </script>
</body>
</html>