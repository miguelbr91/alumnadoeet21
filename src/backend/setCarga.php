<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    $carga=$data['carga'];
    $mail=$data['mail'];
    include_once "conn.php";
    if($carga){
        $sql="UPDATE usuarios SET carga=1 WHERE mail=?";
        $message = "Correcto! La carga de notas ha sido habilitada al usuario $mail";
    }else{
        $sql="UPDATE usuarios SET carga=0 WHERE mail=?";
        $message = "Correcto! La carga de notas ha sido deshabilitada al usuario $mail";
    }
    $carga_sql=$pdo->prepare($sql);
    $carga_sql->execute(array($mail));

    if($carga_sql){
        $error=false;
    }else{
        $error=true;
        $message = "Error! La carga de notas no pudo ser gestionada.";
    }
    $response=[
        "message"=>$message,
        "error"=>$error
    ];
    echo json_encode($response);
?>