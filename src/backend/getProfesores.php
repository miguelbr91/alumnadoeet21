<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    
    include_once "conn.php";

    $sql="SELECT `p`.*,`u`.`idusuarios`,`u`.`carga`,`u`.`acceso`,`u`.`notaCarga` FROM profesores AS `p` INNER JOIN usuarios AS `u` ON `p`.`email`=`u`.`mail`";
    $prof_sql=$pdo->prepare($sql);
    $prof_sql->execute();
    $prof=$prof_sql->fetchAll();

    $response=$prof;
    echo json_encode($response)
?>