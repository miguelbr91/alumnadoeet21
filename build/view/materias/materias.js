new Vue({
    el: '#materias',
    data:{
        titulo: 'Materias',
        usuario:{},
        materias: [],
        ListaAlumnos: false,
        materiaSelect:{},
        alumnos:[],
        print: false,
        loading: false,
        filtroCurso: { "esp":'',"curso":'' },
        ListadoNotas: true,
        dic: false,
        alumnosDic:[],
        mar: false,
        alumnosMar:[],
        alumnosMarExt: []
    },
    methods: {
        getUser(){
            axios.get('backend/session.php')
            .then(resp=>{
                this.usuario=resp.data
                console.log(this.usuario)
                this.getMaterias()
            })
            .catch(e=>{
                console.log(e)
            })
        },
        getMaterias(filtro){
            if(filtro!=undefined){
                console.log(filtro)
            }else{
                console.log('no hay filtro')
            }
            let data= {
                "usuario":this.usuario,
                "filtro":filtro
            }
            axios.post('backend/getMaterias.php', data)
            .then(resp=>{
                console.log(resp.data)
                this.materias=resp.data
            })
            .catch(e=>{
                console.log(e)
            })
        },
        accederCurso(materia){
            console.log(materia)
            this.materiaSelect=materia
            let data=materia
            axios.post('backend/getAlumnosMateria.php', data)
            .then(resp=>{
                console.log(resp.data)
                this.alumnos=resp.data
                    // Cargo el registro de alumnos que se llevan a Diciembre
                    // ------------------------------------------------------
                    
                    let todos=this.alumnos
                    
                    this.alumnosDic=todos.filter(alumno => ( (alumno.NotaFinal == 0) || (alumno.CFin < 6) || (alumno.Nota3T < 6) || (alumno.CodMat==2 && (alumno.Nota1T<6 || alumno.Nota2T<6)) ))
                    console.log(this.alumnosDic)

                    // Cargo el registro de alumnos que se llevan a Marzo
                    // ------------------------------------------------------
                    
                    this.alumnosMar=todos.filter(alumno => ( (alumno.NotaFinal == 0) || (alumno.Mar>0) ))
                    this.alumnosMarExt=todos.filter(alumno => ( (alumno.NotaFinal > 0) && (alumno.FechaNotaFinal>'2019-12-13') && !(alumno.FechaNotaFinal>'2019-12-30') ))
                    console.log('Marzo',this.alumnosMar)
                    console.log('Marzo aprob',this.alumnosMarExt)
            })
            .catch(e=>{
                console.log(e)
            })
        },
        async guardarNotas(notaCarga){
            console.log(this.alumnos)
            let notas = [];
            switch (notaCarga) {
                case "4":
                    notas=this.alumnosDic
                    break;
                case "5":
                    notas=this.alumnosMar
                    break;
                default:
                    notas=this.alumnos
                    break;
            }

            let data = {
                "notaCarga":notaCarga,
                "alumnos": notas
            }
            console.log('data:', data)
            this.loading=true
            await axios.post('backend/cargarNotas.php', data)
            .then(resp=>{
                console.log(resp.data)
                let alerta = document.getElementById('alerta')
                if(resp.data.error){
                    alerta.innerHTML=`
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        ${resp.data.message}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    `
                }else{
                    alerta.innerHTML=`
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        ${resp.data.message}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    `
                }
            })
            .catch(e=>{
                console.log(e)
            })
            this.loading=false
            this.alumnos=[]
            this.materiaSelect={}
            this.ListaAlumnos=false
        },
        async getAlumnosDIC(){
            console.log('alumnos diciembre')
        },
        async getAlumnosMAR(){
            console.log('alumnos marzo')
        },
        imprimirForma(){
            console.log('Imprimir')
            this.print=true
        },
        filtrar(){
            let filtro = {
                "esp": this.filtroCurso.esp,
                "curso": this.filtroCurso.curso,
            }
            this.getMaterias(filtro);
        },
        getInstancias(e){
            switch (e) {
                case 0:
                    this.ListadoNotas=true
                    this.dic=false
                    this.mar=false
                    break;
                case 1:
                    this.ListadoNotas=false
                    this.dic=true
                    this.mar=false
                    break;
                case 2:
                    this.ListadoNotas=false
                    this.dic=false
                    this.mar=true
                    break;
            }
        }
    },
    created() {
        this.getUser()
    },
    mounted() {
    },
    updated() {
        let cargaNotasTot = document.getElementById('cargaNotasTot')
        let cargaNotasDic = document.getElementById('cargaNotasDic')
        let cargaNotasMar = document.getElementById('cargaNotasMar')
        cargaNotasTot.addEventListener('click',()=>{
            cargaNotasTot.classList.add('active')
            cargaNotasDic.classList.remove('active')
            cargaNotasMar.classList.remove('active')
        })
        cargaNotasDic.addEventListener('click',()=>{
            cargaNotasTot.classList.remove('active')
            cargaNotasDic.classList.add('active')
            cargaNotasMar.classList.remove('active')
        })
        cargaNotasMar.addEventListener('click',()=>{
            cargaNotasTot.classList.remove('active')
            cargaNotasDic.classList.remove('active')
            cargaNotasMar.classList.add('active')
        })
    },
})