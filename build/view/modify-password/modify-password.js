new Vue({
    el: '#modify-password',
    data:{
        password:{
            "idusuarios": null,
            "new":null,
            "confirm": null
        },
    },
    methods: {
        getUser(){
            axios.get('backend/session.php')
            .then(resp=>{
                console.log(resp.data)
                this.password.idusuarios=resp.data.idusuarios
            })
            .catch(e=>{
                console.log(e)
            })
        },
        setPassword(){
            if(((this.password.new===null) && (this.password.confirm===null)) || ((this.password.new==='') && (this.password.confirm==='')) ){
                let alerta=document.getElementById('alerta')
                alerta.innerHTML=`
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Error!</strong> Debe ingresar las nuevas contraseñas.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                `
            }else{
                if(this.password.new===this.password.confirm){
                    console.log('cambiar contraseña')
                    console.log(this.password)
                    this.modifyPassword(this.password)
                }else{
                    console.log('Verifique sus contraseñas!')
                    let alerta=document.getElementById('alerta')
                    alerta.innerHTML=`
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>Error!</strong> Las contraseñas no coinciden.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    `
                }
            }
        },
        modifyPassword(password){
            let data = password
            axios.post('backend/modifyPassword.php', data)
            .then(resp=>{
                console.log(resp.data)
                let alerta=document.getElementById('alerta')
                if(resp.data.error){
                    alerta.innerHTML=`
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        ${resp.data.message}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    `
                }else{
                    alerta.innerHTML=`
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        ${resp.data.message}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    `
                    let reload=document.getElementById('reload')
                    reload.classList.remove('d-none')
                    setTimeout(() => {
                        reload.classList.add('d-none')
                        location.reload()
                      }, 2000);
                }
            })
            .catch(e=>{
                console.log(e)
            })
        }
    },
    created() {
        this.getUser()
    },
})