new Vue({
    el: '#app',
    data:{
        title: 'Sistema Web Alumnado',
        session: {
            mail: null,
            password: null,
        }
    },
    methods: {
        getAcces(){
            axios.get('backend/access.php')
            .then(resp=>{
                console.log(resp.data)
                let data = resp.data
                this.session.mail=data.mail
            })
            .catch(e=>{
                console.log(e)
            })
        },
        async ingresar(){
            console.log(this.session)
            let data = this.session
            await axios.post('backend/login.php', data)
            .then(resp=>{
                console.log(resp.data)
                let alerta=document.getElementById('alerta')
                if(resp.data.error===true){
                    alerta.innerHTML=`
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>Error!</strong>${resp.data.message}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    `
                }else{
                    console.log(resp.data)
                    location.reload()
                }
            })
            .catch(e=>{
                console.log(e)
            })
        },
        
    },
    watch: {
        
    },
    created() {
        this.getAcces()
    },
})