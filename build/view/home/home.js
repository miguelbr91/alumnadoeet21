new Vue({
    el:'#home',
    data:{
        titulo: 'Sistema Web Alumnado',
        usuario:{
            "idusuarios": null,
            "mail": null,
            "type": null
        },
    },
    methods: {
        getUser(){
            axios.get('backend/session.php')
            .then(resp=>{
                console.log(resp.data)
                this.usuario=resp.data
                this.loadContent()
            })
            .catch(e=>{
                console.log(e)
            })
        },
        loadContent(){
            let type=this.usuario.type
            let container = document.getElementById('container')
            if(type===0){
                console.log('type:', type)
                $('#container').load('view/modify-password/modify-password.html')
            }else{
                console.log('type:',type)
                $('#container').load('view/materias/materias.html')
            }
        }
    },
    created() {
        this.getUser()
    },
})