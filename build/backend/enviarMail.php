<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    include_once "conn.php";
    $mail=$data['mail'];
    if($mail!=null){
        //envio individual
        $sql="SELECT * FROM usuarios WHERE mail=?";
        $user_sql=$pdo->prepare($sql);
        $user_sql->execute(array($mail));
        $user=$user_sql->fetchAll();

        $key=$user[0]['key'];

        // datos para ingresar al sistema
        $datosIngreso="Este mensaje es solo informativo no responda al mismo, a continuación les brindamos los datos para que acceda al sistema de alumnado.\r\n Aclaraciones: \r\n *   Puede que este e-mail le llegue como correo no deseado o spam, por favor verificar que es un correo seguro, y en caso de recibirlo normalmente informe a los demás docentes para que puedan acceder al sistema de carga de notas. \r\n *   Puede experimentarse errores o fallas de correcta visualización del sistema en navegadores como el internet explorer o Mozilla Firefox, para mayor seguridad le recomendamos usar Google Chrome como navegador predeterminado para utilizar el sistema.\r\n * Ante cualquier inconveniente que tenga con el sistema por favor comuníquese al e-mail: alumnadoeet21@hotmail.com o acérquese al departamento de alumnado. \r\n\r\n * Elance de acceso: http://servicioweb.com.ar/alumnado-web/backend/links.php?mail=$mail \r\n * Contraseña: $key \r\n\r\n --> Los invitamos a dejar sus opiniones y sugerencias acerca de la implementación del sistema a nuestra casilla de correos: alumnadoeet21@hotmail.com";

        // configuracion del mail
        $header="From: alumnadoeet21@hotmail.com \r\n";
        $header.="X-Mailer: PHP/". phpversion() ."\r\n";
        $header.="Mime-Version: 1.0 \r\n";
        $header.="Content-Type: text/plain";

        $msg="Este mensaje fue enviado por: Sistema WEB Alumnado E.E.T. N°21 \r\n";
        $msg.="Asunto: Datos de ingreso al sistema\r\n";
        $msg.="Mensaje: $datosIngreso\r\n";
        $msg.="Enviado el: ".date('d/m/Y', time());

        $to=$mail;
        $asunto='Datos de ingreso al sistema';
        if(mail($to, $asunto, utf8_decode($msg), $header)){
            $message = "Correcto! el mail fue enviado correctamente.";
            $error=false;
        }else{
            $message = "Error! Se produjo un error en el envio.";
            $error=true;
        }

        $response=[
            "mail"=>$mail,
            "message"=>$message,
            "error"=>$error
        ];
    }else{
        //enviar masivo
        $sql="SELECT * FROM usuarios";
        $user_sql=$pdo->prepare($sql);
        $user_sql->execute(array($mail));
        $user=$user_sql->fetchAll();
        $max=sizeof($user);
        for ($i=0; $i < $max; $i++) { 
            $key=$user[$i]['key'];
            $mail=$user[$i]['mail'];


            // datos para ingresar al sistema
            $datosIngreso="Este mensaje es solo informativo no responda al mismo, a continuación les brindamos los datos para que acceda al sistema de alumnado.\r\n Aclaraciones: \r\n *   Puede que este e-mail le llegue como correo no deseado o spam, por favor verificar que es un correo seguro, y en caso de recibirlo normalmente informe a los demás docentes para que puedan acceder al sistema de carga de notas. \r\n *   Puede experimentarse errores o fallas de correcta visualización del sistema en navegadores como el internet explorer o Mozilla Firefox, para mayor seguridad le recomendamos usar Google Chrome como navegador predeterminado para utilizar el sistema.\r\n * Ante cualquier inconveniente que tenga con el sistema por favor comuníquese al e-mail: alumnadoeet21@hotmail.com o acérquese al departamento de alumnado. \r\n\r\n * Elance de acceso: http://servicioweb.com.ar/alumnado-web/backend/links.php?mail=$mail \r\n * Contraseña: $key \r\n\r\n --> Los invitamos a dejar sus opiniones y sugerencias acerca de la implementación del sistema a nuestra casilla de correos: alumnadoeet21@hotmail.com";

            // configuracion del mail
            $header="From: alumnadoeet21@hotmail.com \r\n";
            $header.="X-Mailer: PHP/". phpversion() ."\r\n";
            $header.="Mime-Version: 1.0 \r\n";
            $header.="Content-Type: text/plain";
    
            $msg="Este mensaje fue enviado por: Sistema WEB Alumnado E.E.T. N°21 \r\n";
            $msg.="Asunto: Datos de ingreso al sistema\r\n";
            $msg.="Mensaje: $datosIngreso\r\n";
            $msg.="Enviado el: ".date('d/m/Y', time());
    
            $to=$mail;
            $asunto='Datos de ingreso al sistema';
            if(mail($to, $asunto, utf8_decode($msg), $header)){
                $message = "Correcto! el mail fue enviado correctamente.";
                $error=false;
            }else{
                $message = "Error! Se produjo un error en el envio.";
                $error=true;
            }
    
            $response=[
                "mail"=>$mail,
                "message"=>$message,
                "error"=>$error
            ];
        }
    }

    echo json_encode($response);
?>