<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    include_once "conn.php";
    $notaCarga=$data['notaCarga'];

    $password=null;
    $sql="UPDATE usuarios SET `notaCarga`=?";
    $setInstancia_sql=$pdo->prepare($sql);
    $setInstancia_sql->execute(array($notaCarga));

    if($setInstancia_sql){
        $response=[
            "message"=>"<strong>Correcto!</strong> La instancia de carga de notas se estableció correctamente.",
            "error"=>false
        ];
    }else{
        $response=[
            "message"=>"<strong>Error!</strong> Ocurrió un error en el proceso, vuelva a intentar.",
            "error"=>true
        ];
    }
    echo json_encode($response)
?>