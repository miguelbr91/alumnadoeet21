<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    $user_mail=$data['usuario']['mail'];
    $user_level=$data['usuario']['nivel'];
    $filtro_esp=$data['filtro']['esp'];
    $filtro_curso=$data['filtro']['curso'];
    include_once "conn.php";

    if($user_level==="0"){
        $sql="SELECT DISTINCT(nf.Orden),pm.* FROM profxmat2019 AS pm INNER JOIN notasfinal AS nf ON (pm.CURSO=nf.CURSO AND pm.`DIV`=nf.Division AND pm.ESP=nf.ESPECIALIDAD AND pm.CodMat=nf.CodMat) WHERE Prof_Email=? ORDER BY pm.ESP,pm.CURSO,pm.`DIV`,nf.Orden";
        $mat_sql=$pdo->prepare($sql);
        $mat_sql->execute(array($user_mail));
        $mat_user=$mat_sql->fetchAll();
    }else if($user_level==="2"){
        $sql="SELECT DISTINCT(nf.Orden),pm.* FROM profxmat2019 AS pm INNER JOIN notasfinal AS nf ON (pm.CURSO=nf.CURSO AND pm.`DIV`=nf.Division AND pm.ESP=nf.ESPECIALIDAD AND pm.CodMat=nf.CodMat) WHERE ((pm.ESP LIKE '%$filtro_esp%')AND(pm.CURSO LIKE '%$filtro_curso%')) ORDER BY pm.ESP,pm.CURSO,pm.`DIV`,nf.Orden ";
        $mat_sql=$pdo->prepare($sql);
        $mat_sql->execute(array($user_mail));
        $mat_user=$mat_sql->fetchAll();
    }
    $response=$mat_user;
    echo json_encode($response)
?>