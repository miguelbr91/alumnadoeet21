<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    include_once "conn.php";
    $esp=$data['ESP'];
    $curso=$data['CURSO'];
    $div=$data['DIV'];
    $codMat=$data['CodMat'];
    $sql="SELECT * FROM NotasFinalAlumno WHERE `ESPECIALIDAD`=? AND `CURSO`=? AND `DIVISION` = ? AND `CodMat`=? order by `Apellidos`,`Nombres`";
    $alum_sql=$pdo->prepare($sql);
    $alum_sql->execute(array($esp,$curso,$div,$codMat));
    $alum=$alum_sql->fetchAll();

    $response=$alum;
    echo json_encode($response)
?>