<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    session_start();
    $usr = $_SESSION['idusuarios'];
    if(!empty($usr)){
        $usr = [
            "idusuarios" => $_SESSION['idusuarios'],
            "mail" => $_SESSION['mail'],
            "type" => $_SESSION['type'],
            "nivel"=> $_SESSION['nivel'],
            "carga"=> $_SESSION['carga'],
            "notaCarga"=> $_SESSION['notaCarga'],
        ];
        echo json_encode($usr);
    }else{
        $usr = ["idusuarios" => $_SESSION['idusuarios']];
        echo json_encode($usr);
    }
?>