<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    include_once "conn.php";
    $esp=$data['esp'];
    $cur=$data['cur'];
    $sql="SELECT * FROM cursos WHERE `ESP`=? AND `CURSO`=? ORDER BY `DIV`";
    $cursos_sql=$pdo->prepare($sql);
    $cursos_sql->execute(array($esp,$cur));
    $cursos=$cursos_sql->fetchAll();
    $response=$cursos;
    echo json_encode($response)
?>