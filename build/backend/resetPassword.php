<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    include_once "conn.php";
    $idusuarios=$data['idusuarios'];

    $password=null;
    $sql="UPDATE usuarios SET `password`=?,`acceso`=0 WHERE `idusuarios`=?";
    $reset_sql=$pdo->prepare($sql);
    $reset_sql->execute(array($password,$idusuarios));

    if($reset_sql){
        $response=[
            "message"=>"<strong>Correcto!</strong> La contraseña se reestableció correctamente.",
            "error"=>false
        ];
    }else{
        $response=[
            "message"=>"<strong>Error!</strong> Ocurrió un error en el proceso, vuelva a intentar.",
            "error"=>true
        ];
    }
    echo json_encode($response)
?>