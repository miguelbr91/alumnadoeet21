<?php
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    $json=file_get_contents("php://input");
    $data=json_decode($json, true);
    include_once "conn.php";
    
    $notasAlumnos=$data['alumnos'];
    $notaCarga=$data['notaCarga'];
    $max=sizeof($notasAlumnos);
    $error=false;
    $fech_cierre_trim='2019-11-29';
    $fech_cierre_dic='2019-12-13';
    $fech_cierre_mar='2020-03-13';

    for ($i=0; $i < $max; $i++) {
        $dni=$notasAlumnos[$i]['AlumnoDNI'];
        $esp=$notasAlumnos[$i]['ESPECIALIDAD'];
        $curso=$notasAlumnos[$i]['CURSO'];
        $div=$notasAlumnos[$i]['Division'];
        $codMat=$notasAlumnos[$i]['CodMat'];

        switch ($notaCarga) {
            case 1:
                $sql="UPDATE notasfinal SET `Nota1T`=? WHERE `AlumnoDNI`=? AND `ESPECIALIDAD`=? AND `CURSO`=? AND `DIVISION` = ? AND `CodMat`=?";
                $nota=$notasAlumnos[$i]['Nota1T'];
                break;
            case 2:
                $sql="UPDATE notasfinal SET `Nota2T`=? WHERE `AlumnoDNI`=? AND `ESPECIALIDAD`=? AND `CURSO`=? AND `DIVISION` = ? AND `CodMat`=?";
                $nota=$notasAlumnos[$i]['Nota2T'];
                break;
            case 3:
                $sql="UPDATE notasfinal SET `Nota3T`=? WHERE `AlumnoDNI`=? AND `ESPECIALIDAD`=? AND `CURSO`=? AND `DIVISION` = ? AND `CodMat`=?";
                //Despues de actualizar notas del 3er trimestre debo controlar las diferentes condiciones del alumno
                $nota=$notasAlumnos[$i]['Nota3T'];
                break;
            case 4:
                $sql="UPDATE notasfinal SET `Dic`=? WHERE `AlumnoDNI`=? AND `ESPECIALIDAD`=? AND `CURSO`=? AND `DIVISION` = ? AND `CodMat`=?";
                $nota=$notasAlumnos[$i]['Dic'];
                break;
            case 5:
                $sql="UPDATE notasfinal SET `Mar`=? WHERE `AlumnoDNI`=? AND `ESPECIALIDAD`=? AND `CURSO`=? AND `DIVISION` = ? AND `CodMat`=?";
                $nota=$notasAlumnos[$i]['Mar'];
                break;
        }

        $updateAlum_sql=$pdo->prepare($sql);
        $updateAlum_sql->execute(array($nota,$dni,$esp,$curso,$div,$codMat));
        if(!$updateAlum_sql){
            $error=true;
        }

        // Proceso de actualizacion de promedios y aprobacion
        if($notaCarga<4){
            $cond=false; //condicional que me verifica que el alumno esta aprobado en la materia
            // Verificar que los 3 trimestres tienen cargada la nota para poder sacar un promedio
            if( ($notasAlumnos[$i]['Nota1T']>0) && ($notasAlumnos[$i]['Nota2T']>0) && ($notasAlumnos[$i]['Nota3T']>0) ){
                $cond=true; //considero previamente que el alumno aprueba la materia, luego se verifica lo contrario
                $prom=($notasAlumnos[$i]['Nota1T'] + $notasAlumnos[$i]['Nota2T'] + $notasAlumnos[$i]['Nota3T'])/3;
                $prom=floor(($prom*100))/100;
                // Si la nota del 3er trimestre es menor a 6 desaprueba la materia
                if($notasAlumnos[$i]['Nota3T']<6){
                    $cond = false;
                }
                // Si el promedio de los trimestres es menor a 6 desaprueba la materia
                if($prom<6){
                    $cond = false;
                }
                // En el caso de los Talleres si el promedio de los trimestes es mayor a 6 pero en algun trimestre saco nota menor a 6 desaprueba
                if( ($prom>=6) && ($notasAlumnos[$i]['CodMat']==2) ){
                    if(($notasAlumnos[$i]['Nota1T']<6) || ($notasAlumnos[$i]['Nota2T']<6) || ($notasAlumnos[$i]['Nota3T']<6)){
                        $cond=false;
                    }
                }
    
                if($cond){
                    //alumno aprueba la materia
                    $sql="UPDATE notasfinal SET `CFin`=?,`NotaFinal`=?,`FechaNotaFinal`=?,`CodAprob`=? WHERE `AlumnoDNI`=? AND `CodMat`=? AND `ESPECIALIDAD`=? AND `CURSO`=? AND `DIVISION` = ?";
                    $cfin=$prom;
                    $nf=$prom;
                    $fech_nf=$fech_cierre_trim;
                    $codAprob=2;
                    // actualizo registro con el promedio
                    $updateAlum_sql=$pdo->prepare($sql);
                    $updateAlum_sql->execute(array($cfin,$nf,$fech_nf,$codAprob,$dni,$codMat,$esp,$curso,$div));
                    if(!$updateAlum_sql){
                        $error=true;
                    }
                }
                if(!$cond){
                    // alumno desaprueba materia por no superar el promedio o por tener desaprobado el 3er Trim
                    $sql="UPDATE notasfinal SET `CFin`=?,`NotaFinal`=?,`FechaNotaFinal`=?,`CodAprob`=? WHERE `AlumnoDNI`=? AND `CodMat`=? AND `ESPECIALIDAD`=? AND `CURSO`=? AND `DIVISION` = ?";
                    $cfin=$prom;
                    $nf=0;
                    $fech_nf=null;
                    $codAprob=0;
                    // actualizo registro con el promedio
                    $updateAlum_sql=$pdo->prepare($sql);
                    $updateAlum_sql->execute(array($cfin,$nf,$fech_nf,$codAprob,$dni,$codMat,$esp,$curso,$div));
                    if(!$updateAlum_sql){
                        $error=true;
                    }
                }
            }else{
                // En caso de no tener todas las notas o de quitar una nota se debe resetrar el promedio y la nota final
                $sql="UPDATE notasfinal SET `CFin`=?,`NotaFinal`=?,`FechaNotaFinal`=?,`CodAprob`=? WHERE `AlumnoDNI`=? AND `CodMat`=? AND `ESPECIALIDAD`=? AND `CURSO`=? AND `DIVISION` = ?";
                $cfin=0;
                $nf=0;
                $fech_nf=null;
                $codAprob=0;
                // actualizo registro con el promedio
                $updateAlum_sql=$pdo->prepare($sql);
                $updateAlum_sql->execute(array($cfin,$nf,$fech_nf,$codAprob,$dni,$codMat,$esp,$curso,$div));
                if(!$updateAlum_sql){
                    $error=true;
                }
            }
        }

        //ACTUALIZA NOTA FINAL CON CARGA DE NOTAS DICIEMBRE
        if($notaCarga==4){
            if($notasAlumnos[$i]['Dic']>=6){
                if($notasAlumnos[$i]['CFin']>0){
                    $prom=($notasAlumnos[$i]['CFin']+$notasAlumnos[$i]['Dic'])/2;
                    $prom=floor(($prom*100))/100;
                    if($prom>=3.5 && $prom<4){
                        $prom=4;
                    }
                    $nf=$prom;
                }else{
                    $nf=$notasAlumnos[$i]['Dic'];
                }
                $codAprob=9;
                $fech_nf=$fech_cierre_dic;
                $sql="UPDATE notasfinal SET `NotaFinal`=?,`FechaNotaFinal`=?,`CodAprob`=? WHERE `AlumnoDNI`=? AND `CodMat`=? AND `ESPECIALIDAD`=? AND `CURSO`=? AND `DIVISION` = ?";
                // actualizo registro con el promedio
                $updateAlum_sql=$pdo->prepare($sql);
                $updateAlum_sql->execute(array($nf,$fech_nf,$codAprob,$dni,$codMat,$esp,$curso,$div));
            }else{
                // En caso de desaprobar en diciembre o si se quita la nota de diciembre
                // se debe actualizar la nota final a 0 y tambien al cond de aprob
                $nf=0;
                $codAprob=0;
                $fech_nf=null;
                $sql="UPDATE notasfinal SET `NotaFinal`=?,`FechaNotaFinal`=?,`CodAprob`=? WHERE `AlumnoDNI`=? AND `CodMat`=? AND `ESPECIALIDAD`=? AND `CURSO`=? AND `DIVISION` = ?";
                // actualizo registro con el promedio
                $updateAlum_sql=$pdo->prepare($sql);
                $updateAlum_sql->execute(array($nf,$fech_nf,$codAprob,$dni,$codMat,$esp,$curso,$div));
            }
        }

        //ACTUALIZA NOTA FINAL CON CARGA DE NOTAS MARZO
        if($notaCarga==5){
            if($notasAlumnos[$i]['Mar']>=6){
                $nf=$notasAlumnos[$i]['Mar'];
                $codAprob=9;
                $fech_nf=$fech_cierre_mar;
                $sql="UPDATE notasfinal SET `NotaFinal`=?,`FechaNotaFinal`=?,`CodAprob`=? WHERE `AlumnoDNI`=? AND `CodMat`=? AND `ESPECIALIDAD`=? AND `CURSO`=? AND `DIVISION` = ?";
                // actualizo registro con la nota obtenida en la mesa de marzo, no se promedia notas en esta instancia
                $updateAlum_sql=$pdo->prepare($sql);
                $updateAlum_sql->execute(array($nf,$fech_nf,$codAprob,$dni,$codMat,$esp,$curso,$div));
            }else{
                // En caso de desaprobar en marzo o si se quita la nota de marzo
                // se debe actualizar la nota final a 0 y tambien al cond de aprob
                $nf=0;
                $codAprob=0;
                $fech_nf=null;
                $sql="UPDATE notasfinal SET `NotaFinal`=?,`FechaNotaFinal`=?,`CodAprob`=? WHERE `AlumnoDNI`=? AND `CodMat`=? AND `ESPECIALIDAD`=? AND `CURSO`=? AND `DIVISION` = ?";
                // actualizo registro
                $updateAlum_sql=$pdo->prepare($sql);
                $updateAlum_sql->execute(array($nf,$fech_nf,$codAprob,$dni,$codMat,$esp,$curso,$div));
            }
        }
    }

    if(!$error){
        $alum=[
            "message"=>"<strong>Correcto!</strong> La carga de notas se realizo correctamente.",
            "error"=>$error
        ];
    }else{
        $alum=[
            "message"=>"<strong>Error!</strong> La carga de notas no se pudo realizar.",
            "error"=>$error
        ];
    }
    

    $response=$alum;
    echo json_encode($response)
?>